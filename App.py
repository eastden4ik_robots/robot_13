import datetime

import vk_api
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id
from vk_api.longpoll import VkLongPoll, VkEventType

from Models import CountryCodes, CountryCodesModel

from pykson import Pykson

from Utils import get_country_info, get_all_info
from Config import TOKEN

if __name__ == '__main__':
    vk_session = vk_api.VkApi(token=TOKEN)
    vk = vk_session.get_api()

    keyboard = VkKeyboard(one_time=True)
    countries = []

    for cntry in CountryCodes:
        countries.append(Pykson().from_json(cntry.value, CountryCodesModel).name)

    if len(countries) % 4 != 0:
        rows = len(countries) // 4
    else:
        rows = len(countries) // 4 + 1

    k = 0
    for cntry in countries:
        if k != len(countries) - 1:
            keyboard.add_button(cntry, color=VkKeyboardColor.DEFAULT)
            keyboard.add_line()
            k += 1
        else:
            keyboard.add_button(cntry, color=VkKeyboardColor.DEFAULT)

    print(keyboard.lines)
    longpoll = VkLongPoll(vk_session)

    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                if event.text == 'старт':
                    vk.messages.send(
                        peer_id=event.peer_id,
                        random_id=get_random_id(),
                        keyboard=keyboard.get_keyboard(),
                        message='Доступные страны'
                    )
                elif event.text in countries:
                    for cntry in CountryCodes:
                        if event.text == Pykson().from_json(cntry.value, CountryCodesModel).name:
                            code = Pykson().from_json(cntry.value, CountryCodesModel).code
                            result = 'Данные для ' + event.text + '\n'
                            if code != 'ALL':
                                countryInfo = get_country_info(code=code)
                            else:
                                countryInfo = get_all_info(code=code)
                            result += ' 🔴 Всего заболело: ' + str(countryInfo.total_cases) + '\n'
                            result += ' ✅ Всего вылечилось: ' + str(countryInfo.total_recovered) + '\n'
                            result += ' ❌ Всего смертей: ' + str(countryInfo.total_deaths) + '\n'
                            result += ' ⚠️ Новые случаи за сегодня: ' + \
                                      str(countryInfo.total_new_cases_today) + '\n'
                            result += ' ⚠️ Новые смерти за сегодня: ' + \
                                      str(countryInfo.total_new_deaths_today) + '\n'
                            delta = datetime.timedelta(hours=3,
                                                       minutes=0)
                            t = (datetime.datetime.now(
                                datetime.timezone.utc) + delta)
                            now_time = t.strftime("%H:%M:%S")
                            now_date = t.strftime("%d.%m.%Y")
                            result += "Данные актуальны на [" + str(now_date) + " " + str(now_time) + "]."
                            vk.messages.send(
                                peer_id=event.peer_id,
                                random_id=get_random_id(),
                                message=result,
                                keyboard=keyboard.get_keyboard()
                            )
                            break
                else:
                    vk.messages.send(
                                peer_id=event.peer_id,
                                random_id=get_random_id(),
                                message='Введите команду: старт'
                    )