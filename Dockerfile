FROM python:3.8-slim

ARG PY_FYLES=./

WORKDIR /usr/local/runme

COPY ${PY_FYLES} ./

RUN pip install -r requiremmnets.txt
RUN python core/App.py