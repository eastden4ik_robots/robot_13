import requests
from pykson import Pykson

from Models import TotalCountry, TotalCases


def get_country_info(code: str):
    response = requests.get("https://api.thevirustracker.com/free-api?countryTotal=" + code)
    russiaCases = Pykson().from_json(response.json(), TotalCountry)
    return russiaCases.countrydata[0]


def get_all_info(code: str):
    response = requests.get("https://api.thevirustracker.com/free-api?global=stats")
    totalCases = Pykson().from_json(response.json(), TotalCases)
    return totalCases.results[0]
