from pykson import Pykson, JsonObject, IntegerField, StringField, ObjectListField, ObjectField
import requests
from enum import Enum


class Source(JsonObject):
    url = StringField()


class TotalCase(JsonObject):
    total_cases = IntegerField()
    total_recovered = IntegerField()
    total_deaths = IntegerField()
    total_unresolved = IntegerField()
    total_new_cases_today = IntegerField()
    total_new_deaths_today = IntegerField()
    total_active_cases = IntegerField()
    total_serious_cases = IntegerField()
    total_affected_countries = IntegerField()
    source = ObjectField(Source)


# Model of getting total amount of covid-19 statistics
class TotalCases(JsonObject):
    results = ObjectListField(TotalCase)
    stat = StringField()


class Info(JsonObject):
    ourid = IntegerField()
    title = StringField()
    code = StringField()
    source = StringField()


class CountryInfo(JsonObject):
    info = ObjectField(Info)
    total_cases = IntegerField()
    total_recovered = IntegerField()
    total_deaths = IntegerField()
    total_unresolved = IntegerField()
    total_new_cases_today = IntegerField()
    total_new_deaths_today = IntegerField()
    total_active_cases = IntegerField()
    total_serious_cases = IntegerField()
    total_danger_rank = IntegerField()


# Model of getting information for each country
class TotalCountry(JsonObject):
    countrydata = ObjectListField(CountryInfo)
    stat = StringField()


class CountryCodes(Enum):
    All = {'code': 'ALL', 'name': 'Весь мир'}
    Russia = {'code': 'RU', 'name': 'Россия'}
    Usa = {'code': 'US', 'name': 'США'}
    Ukraine = {'code': 'UA', 'name': 'Украина'}
    Canada = {'code': 'CA', 'name': 'Канада'}
    China = {'code': 'CN', 'name': 'Китай'}
    Germany = {'code': 'DE', 'name': 'Германия'}
    Italy = {'code': 'IT', 'name': 'Италия'}
    Spain = {'code': 'ES', 'name': 'Испания'}


class CountryCodesModel(JsonObject):
    code = StringField()
    name = StringField()

# if __name__ == '__main__':
#     responce = requests.get("https://api.thevirustracker.com/free-api?global=stats")
#     totalCases = Pykson().from_json(responce.json(), TotalCases)
#     print("-------")
#     print(responce.json())
#     print("-------")
#     print(totalCases.stat)
#     print("-------")
#     print("" + str(totalCases.results[0].total_cases))
#     responce = requests.get("https://api.thevirustracker.com/free-api?countryTotal=" + CountryCodes.Germany.value)
#     russiaCases = Pykson().from_json(responce.json(), TotalCountry)
#     print(russiaCases.countrydata[0].info.code)
